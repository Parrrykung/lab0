using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace paritud.gamedev3.ep2
{
    public class ControlObjectMovementOnXZPlaneUsingArrowKeys : StepMovement
    {
        // Start is called before the first frame update
        void Start()
        {
        
        }

        // Update is called once per frame
        void Update()
        {
            //GetKey
             if (Input.GetKey(KeyCode.LeftArrow))
                 {
                 MoveLeft();
                 }
             else if (Input.GetKey(KeyCode.RightArrow))
                 {
                 MoveRight();
                 }
             else if (Input.GetKey(KeyCode.UpArrow))
                 {
                 MoveForward();
                 }
             else if (Input.GetKey(KeyCode.DownArrow))
             {
                 MoveBackward();
             }
        }
    }
} 

