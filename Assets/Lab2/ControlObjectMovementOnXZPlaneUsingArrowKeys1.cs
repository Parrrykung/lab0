using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace paritud.gamedev3.ep2.InputSystem
{
    public class ControlObjectMovementOnXZPlaneUsingArrowKeys1 : StepMovement
    {
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            Keyboard keyboard = Keyboard.current;
            //GetKey
            if (keyboard[Key.LeftArrow].isPressed)
            {
                MoveLeft();
            }
            else if (keyboard[Key.RightArrow].isPressed)
            {
                MoveRight();
            }
            else if (keyboard[Key.UpArrow].isPressed)
            {
                MoveForward();
            }
            else if (keyboard[Key.DownArrow].isPressed)
            {
                MoveBackward();
            }
        }
    }
}
