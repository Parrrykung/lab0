using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace paritud.gamedev3.ep2
{
    public class ControlObjectMovementOnXZPlaneUsingWASD : StepMovement
    {
        // Start is called before the first frame update
        void Start()
        {
        
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.A))
            {
                MoveLeft();
            }
            else if (Input.GetKeyDown(KeyCode.D))
            {
                MoveRight();
            }
            else if (Input.GetKeyDown(KeyCode.W))
            {
                 MoveForward();
                 }
             else if (Input.GetKeyDown(KeyCode.S))
                 {
                 MoveBackward();
                 }
        }
    }
}

