using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace paritud.gamedev3.ep1
{
    public class ItemTypeComponent : MonoBehaviour
    {
        [SerializeField]
        protected ItemType.itemtype m_ItemType;
        public ItemType.itemtype Type 
        {
            get
            {
                return m_ItemType;
            }
            set
            {
                m_ItemType = value;
            
            }
        }
    }
}

