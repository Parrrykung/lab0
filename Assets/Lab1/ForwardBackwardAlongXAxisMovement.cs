using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace paritud.gamedev3.ep1
{
    public class ForwardBackwardAlongXAxisMovement : MonoBehaviour
    {
        // Start is called before the first frame update
        public const float MAX_MOVEMENT_DISTANCE = 7.0f; 
        
        private float _displacementCounter = 0;
        
        [SerializeField]
        private float _xComponentSpeed = 0.02f;
        private Vector3 _currentMovementSpeed = Vector3.zero;
        void Start()
        {
            _currentMovementSpeed.x = _xComponentSpeed;
        }

        // Update is called once per frame
        void Update()
        {
            transform.position += _currentMovementSpeed;
            _displacementCounter += _currentMovementSpeed.x;
            if (Mathf.Abs(_displacementCounter) > MAX_MOVEMENT_DISTANCE)
            {
                _displacementCounter = 0;
                _currentMovementSpeed *= -1;
            }
        }
    }
}

