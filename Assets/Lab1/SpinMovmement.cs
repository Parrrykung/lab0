using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Serialization;

namespace paritud.gamedev3.ep1
{
    public class SpinMovmement : MonoBehaviour
    {
            [SerializeField] private float _angularSpeed = 5.0f;
            [SerializeField] private Vector3 _axisOfRotation = new Vector3(1.0f, 0, 0);

            private Transform _objTransformComponent;

            // Start is called before the first frame update
            void Start()
            {
                _objTransformComponent = this.gameObject.GetComponent<Transform>();
            }

            // Update is called once per frame
            void Update()
            {
                _objTransformComponent.Rotate(_axisOfRotation, _angularSpeed);
            }
        }
    }

